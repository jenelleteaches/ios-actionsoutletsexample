//
//  ViewController.swift
//  SimpleApp
//
//  Created by MacStudent on 2018-11-07.
//  Copyright © 2018 parrot. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: OUTLETS --> document.getElementByID(....)
    //---------------------------
    @IBOutlet weak var amtBox: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    
    //MARK: Default functions
    //---------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: OnClick functions --> ACTION
    //---------------------------
    
    @IBAction func calculateButtonPressed(_ sender: Any) {
        print("Button pressed!");
        
        // 1. get value from text box
        let amt = amtBox.text;
        print("Amount entered: \(amt)")
        
        
        // 2. convert to an integer
        let amt2 = Double(amt!)
        
        // 3. do some math
        let tax = amt2! * 0.13;
        let total = amt2! + tax;
        
        // 4. output to console / UI
        print(total)
        
        // GET VALUE FROM UI
        resultLabel.text = "Final Amount \(total)"
            
        
        
    }
    


}

